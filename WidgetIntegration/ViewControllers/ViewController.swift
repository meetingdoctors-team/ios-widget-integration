//
//  ViewController.swift
//  WidgetIntegration
//
//  Created by Edgar Paz Moreno on 08/09/2020.
//  Copyright © 2020 MeetingDoctors. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var firebaseToken: String? {
        didSet {
            if self.webView != nil, let token = self.firebaseToken {
                self.webView?.setPushToken(token)
            }
        }
    }
    
    var webView: MeetingDoctorsWebViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.addButton()
    }
    
    func addButton() {
        let button = UIButton()
        button.setTitle("launch webview", for: .normal)
        button.addTarget(self, action: #selector(launchWebView), for: .touchUpInside)
        button.setTitleColor(.black, for: .normal)
        self.view.addFullSubview(button)
    }
    
    @objc func launchWebView() {
        self.webView = MeetingDoctorsWebViewController()
        self.webView?.delegate = self
        self.webView?.url = "https://sdk.dev.meetingdoctors.com"
//        self.webView?.url = "https://e9323ea56964.ngrok.io"
        self.webView?.modalPresentationStyle = .fullScreen
        if let webView = self.webView {
            self.present(webView, animated: true, completion: {
                if let token = self.firebaseToken {
                    self.webView?.setPushToken(token)
                }
            })
        }
    }


}

extension ViewController: MeetingDoctorsWebViewStatus {
    func error(_ error: Error) {
        print(error)
    }
}

extension ViewController: UNUserNotificationCenterDelegate {
    public func userNotificationCenter(_ userNotificationCenter: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let webView = self.webView {
            webView.processPush(response)
        }
    }
}
