//
//  MeetingDoctorsWebViewController.swift
//  WidgetIntegration
//
//  Created by Edgar Paz Moreno on 08/09/2020.
//  Copyright © 2020 MeetingDoctors. All rights reserved.
//

import UIKit
import WebKit

enum MeetingDoctorsWebViewError: Error {
    case InvalidURL
    case WebViewNotInitialized
}

protocol MeetingDoctorsWebViewStatus: NSObject {
    func error(_ error: Error)
}

class MeetingDoctorsWebViewController: UIViewController {
    fileprivate var webView: WKWebView?
    weak var delegate: MeetingDoctorsWebViewStatus?
    
    var url: String = ""
    var isFirstTime = true
    
    private var webviewObs = Set<NSKeyValueObservation>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareWebView()
        
        self.load(self.url)
        
    }
    
    func prepareDebugButton() {
        let button = UIButton(frame: CGRect(x: 30, y: 50, width: 150, height: 30))
        button.setTitle("processPayload", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(launchProcessPush), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    @objc func launchProcessPush() {
        self.initialize()
        
        
//        self.processPush(["data" : ["data" : ["type" : "message"]]])
        
//        self.processPush(["data" :
//                            ["data" :
//                                [   "type" : "video_consultation:assigned",
//                                    "room_id" : 560809,
//                                    "professional": ["name": "gerard",
//                                                    "avatar": "https://s3.eu-central-1.amazonaws.com/meetingdocto…200325163823_6df9aefb-cb9f-4bce-bfb5-b172eba1b9e5",
//                                                    "hash": "2a751a88-104f-48ca-9d64-68b0fcda6eb5"]
//                                ]
//                            ]
//                        ])
        
//        self.processPush(["data" : ["data" : ["type" : "video_call:calling"]]])
//        self.processPush(["data" : ["data" : ["type" : "video_consultation:cancelled"]]])
//        self.processPush(["data" : ["data" : ["type" : "message_created"]]])
//        self.processPush(["data" : ["data" : [["type" : "video_consultation:assigned"]]])
        
//        self.processPush(["data" :
//                            ["data" : [
//                                "customer": ["hash": "42fbd5c2-8432-4cc0-bd77-55b661030f43"],
//                                "message": ["title": "gerard",
//                                            "body": "Tienes mensajes nuevos sin leer"
//                                ],
//                                "message_payload": ["createdAt": "2020-09-16T13:40:38+00:00",
//                                                    "auto": 0,
//                                                    "string": "sd",
//                                                    "messageId": "a3085126-cb75-4314-b983-261f86438a0a",
//                                                    "id": 4061072],
//                                "module": "consultations",
//                                "professional": ["name": "gerard",
//                                                 "avatar": "https://s3.eu-central-1.amazonaws.com/meetingdocto…200325163823_6df9aefb-cb9f-4bce-bfb5-b172eba1b9e5",
//                                                 "hash": "2a751a88-104f-48ca-9d64-68b0fcda6eb5"],
//                                "room_id": 560809,
//                                "type": "message_created"
//                                ]
//                            ]
//                        ])
        
        
        
        
    }
    
    // initialization
    
    private func prepareWebView() {
        self.webView = WKWebView()
        guard let webView = self.webView else {
            self.delegate?.error(MeetingDoctorsWebViewError.WebViewNotInitialized)
            return
        }
        self.view.addFullSubview(webView)
        self.prepareDebugButton()
//        if let webView = self.webView {
//            self.webviewObs.insert(webView.observe(\.isLoading, options: .new, changeHandler: { _, ch in
//                if let isloading = ch.newValue, !isloading, self.isFirstTime {
//                    self.isFirstTime = false
//                    self.initialize()
//                }
//            }))
//        }
    }
    
    private func load(_ url: String) {
        guard let finalUrl = URL(string: url) else {
            self.delegate?.error(MeetingDoctorsWebViewError.InvalidURL)
            return
        }
        guard let webView = self.webView else {
            self.delegate?.error(MeetingDoctorsWebViewError.WebViewNotInitialized)
            return
        }
        webView.load(URLRequest(url: finalUrl))
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}

extension MeetingDoctorsWebViewController {
    func setPushToken(_ fcmToken: String) {
        let javascript = "window.meetingdoctors.setPushToken(\(fcmToken))"
        self.webView?.evaluateJavaScript(javascript, completionHandler: { (_, error) in
            if let error = error {
                self.delegate?.error(error)
            }
        })
    }
    
    func initialize() {
//        let payload = "{\"apiKey\": 'jmUCkN9AqnY25RJS', \"displayMode\": 'widget', \"language\": 'es', \"jwt\":\"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jdXN0b21lci5kZXYubWVldGluZ2RvY3RvcnMuY29tXC93aWRnZXRcL3YzXC9lbWFpbC12ZXJpZmljYXRpb25cL2NoZWNrIiwiaWF0IjoxNjAwMjY1Njg2LCJleHAiOjE2MDAyNjkyODYsIm5iZiI6MTYwMDI2NTY4NiwianRpIjoidnBjRkNRMExHNE9xS2NjMSIsInN1YiI6IjAxZGYzNWFhLTVjODQtNDc2My1iOWNmLWE5ZTU3MGY3ZTZlZSIsInBydiI6IjJjNjNiYWVjY2IxYTE5ZDk1MmU5OTZmOWYwMTNkMzU3ODFmMGI5YTciLCJtZXRhIjp7InR5cGUiOiJjdXN0b21lciIsIm5hbWUiOm51bGwsImdlbmRlciI6bnVsbCwiY291bnRyeV9jb2RlIjoiZXMiLCJiaXJ0aF9kYXRlIjp7ImRhdGUiOiIxOTcwLTAxLTAxIDAxOjAwOjAwLjAwMDAwMCIsInRpbWV6b25lX3R5cGUiOjMsInRpbWV6b25lIjoiRXVyb3BlXC9NYWRyaWQifSwiYXBpX2tleSI6ImptVUNrTjlBcW5ZMjVSSlMiLCJjb3ZlcmFnZV9uYW1lIjoiY292ZXJhZ2VfbmFtZSJ9fQ.AgSJu4BUxdi8IZ_me4deQtMFq9I0M4jOgf4AsshaPAM\", \"isEmbeddedApp\": true, \"defaultOpen\": true,}"
        
//        let payload: [String : Any] = ["apiKey" : "jmUCkN9AqnY25RJS",
//                       "displayMode" : "widget",
//                       "language" : "es",
//                       "jwt" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9jdXN0b21lci5kZXYubWVldGluZ2RvY3RvcnMuY29tXC93aWRnZXRcL3YzXC9lbWFpbC12ZXJpZmljYXRpb25cL2NoZWNrIiwiaWF0IjoxNjAwMjY1Njg2LCJleHAiOjE2MDAyNjkyODYsIm5iZiI6MTYwMDI2NTY4NiwianRpIjoidnBjRkNRMExHNE9xS2NjMSIsInN1YiI6IjAxZGYzNWFhLTVjODQtNDc2My1iOWNmLWE5ZTU3MGY3ZTZlZSIsInBydiI6IjJjNjNiYWVjY2IxYTE5ZDk1MmU5OTZmOWYwMTNkMzU3ODFmMGI5YTciLCJtZXRhIjp7InR5cGUiOiJjdXN0b21lciIsIm5hbWUiOm51bGwsImdlbmRlciI6bnVsbCwiY291bnRyeV9jb2RlIjoiZXMiLCJiaXJ0aF9kYXRlIjp7ImRhdGUiOiIxOTcwLTAxLTAxIDAxOjAwOjAwLjAwMDAwMCIsInRpbWV6b25lX3R5cGUiOjMsInRpbWV6b25lIjoiRXVyb3BlXC9NYWRyaWQifSwiYXBpX2tleSI6ImptVUNrTjlBcW5ZMjVSSlMiLCJjb3ZlcmFnZV9uYW1lIjoiY292ZXJhZ2VfbmFtZSJ9fQ.AgSJu4BUxdi8IZ_me4deQtMFq9I0M4jOgf4AsshaPAM",
//                       "isEmbeddedApp" : true,
//                       "defaultOpen" : true
//                        ]
        
        let payload: [String : Any] = ["apiKey" : "jmUCkN9AqnY25RJS",
                                        "displayMode" : "widget",
                                        "loginEmail" : [],
                                        "language" : "es",
                                        "defaultOpen" : true
                                         ]
        
        do {
            let data = try JSONSerialization.data(withJSONObject: payload, options: [])
            if let jsonString = String(data: data, encoding: .utf8) {
                let javascript = "window.meetingdoctors.initialize(\(jsonString))"
                self.webView?.evaluateJavaScript(javascript, completionHandler: { (_, error) in
                    if let error = error {
                        self.delegate?.error(error)
                    }
                })
            }
        } catch {
            self.delegate?.error(error)
        }
//        window.meetingdoctors.initialize({
//          apiKey: ‘jmUCkN9AqnY25RJS’,
//          displayMode: ‘widget’, // one of [fullScreen, contained, widget]
//          loginEmail: [],
//          language: ‘es’,
//        });
        
        
    }
    
    func processPush(_ payload: [AnyHashable : Any]) {
        do {
            let data = try JSONSerialization.data(withJSONObject: payload, options: [])
            if let jsonString = String(data: data, encoding: .utf8) {
                let javascript = "window.meetingdoctors.processPush('\(jsonString)')"
//                let javascript = "window.meetingdoctors.processPush(\"{'data':{'data':{'type':'message'}}}\")"
//                let javascript = "window.meetingdoctors.processPush('prueba')"
                
                self.webView?.evaluateJavaScript(javascript, completionHandler: { (_, error) in
                    if let error = error {
                        self.delegate?.error(error)
                    }
                })
            }
        } catch {
            self.delegate?.error(error)
        }
    }
    
    func processPush(_ response: UNNotificationResponse) {
        let userInfo = response.notification.request.content.userInfo
        self.processPush(userInfo)
    }
}

// window.meetingdoctors.loQueSea

//registra el token
//const setPushToken = token => {
//  // me pasa la push
//  //yield Napi.notification.register({ token: token, os: osName });
//}
////nos pasa la push
//const processPush = params => {
//  // aqui llamamos a nuestro canal de notificaciones
//  // hacer un emit hacial el canal
//  console.log(‘>>>> Params: ’, params)
//}





//<script>
//  document.addEventListener('DOMContentLoaded', function() {
//    window.meetingdoctors.initialize({
//      apiKey: 'jmUCkN9AqnY25RJS',
//      displayMode: 'widget', // one of [fullScreen, contained, widget]
//      loginEmail: [],
//      language: 'es',
//
//      // new aditional params
//      isEmbeddedApp: true,
//
//    });
//  }, false);
//</script>
